(function (window, k) {
    if (!window.AppstleIncluded && (!urlIsProductPage() || 'V1' === 'V2')) {
      window.AppstleIncluded = true;
      appstleLoadScript = function (src, callback) {
        var script = document.createElement("script");
        script.charset = "utf-8";
            script.async = true;
        script.src = src;
        script.onload = script.onreadystatechange = function () {
          if (!script.readyState || /loaded|complete/.test(script.readyState)) {
            script.onload = script.onreadystatechange = null;
            script = k;
            callback && callback();
          }
        };
            document.getElementsByTagName("head")[0].appendChild(script)
      };


      appstleLoadScript("https://cdn.shopify.com/s/files/1/0607/2454/3697/t/41/assets/appstle-subscription.js?v=1653291701");

      window.RS = Window.RS || {};
      RS.Config = {
        "selectors": {
            "payment_button_selectors": "form[action$='/cart/add'] .shopify-payment-button",
            "subscriptionLinkSelector": "#MainContent > div > header",
            "atcButtonPlacement": "BEFORE",
            "subscriptionLinkPlacement": "BEFORE",
            "cartRowSelector": "",
            "cartLineItemSelector": "",
            "cartLineItemPerQuantityPriceSelector": "",
            "cartLineItemTotalPriceSelector": "",
            "cartLineItemSellingPlanNameSelector": "",
            "cartSubTotalSelector" : "",
            "cartLineItemPriceSelector": "",
        },
        "enableCartWidgetFeature": "false",
        "useUrlWithCustomerId": "false",
        "atcButtonSelector": ".btn.btn--full.add-to-cart",
        "moneyFormat": "{% raw %}CHF {{amount}}{% endraw %}",
        "oneTimePurchaseText": "EINMALIGER KAUF",
        "shop": "bethechangeswiss.myshopify.com",
        "deliveryText": "delivery",
        "purchaseOptionsText": "Kaufoptionen",
        "manageSubscriptionButtonText": "Manage Subscription",
        "subscriptionOptionText": "ABONNIEREN ",
        "sellingPlanSelectTitle": "LIEFERFREQUENZ",
        "subscriptionPriceDisplayText": "",
        "tooltipTitle": "Details zum Abonnement",
        "api_key": "",
        "showTooltipOnClick": "false",
        "tooltipDesctiption": "<strong>Behalten Sie die volle Kontrolle \u00FCber Ihre Abonnements<\/strong><br\/><br\/>\u00DCberspringen, verschieben, bearbeiten, stornieren Sie Lieferungen jederzeit nach Ihrem Bedarf. Mindestlaufzeit sind 3 Lieferungen.",
        "tooltipDescriptionOnPrepaidPlan": "<b>Details zum Prepaid Plan<\/b><\/br> Gesamtpreis: {{totalPrice}} ( Preis f\u00FCr jede Lieferung: {{pricePerDelivery}})",
        "tooltipDescriptionOnMultipleDiscount": "<b>Details zum Rabatt<\/b><\/br> Der anf\u00E4ngliche Rabatt betr\u00E4gt {{discountOne}} und dann {{discountTwo}}",
        "tooltipDescriptionCustomization": "{{{defaultTooltipDescription}}} <\/br>  {{{prepaidDetails}}} <\/br> {{{discountDetails}}}",
        "orderStatusManageSubscriptionTitle": "Abonnement",
        "orderStatusManageSubscriptionDescription": "Gehen Sie zu Ihrem Konto, um Ihre Abonnements einzusehen und zu verwalten. Bitte verwenden Sie die gleiche E-Mail-Adresse, die Sie f\u00FCr den Kauf des Abonnements verwendet haben.",
        "orderStatusManageSubscriptionButtonText": "Verwalten Sie Ihr Abonnement",
        "subscriptionOptionSelectedByDefault" : false,
        "totalPricePerDeliveryText" : "{{prepaidPerDeliveryPrice}}\/delivery",
        "memberOnlySellingPlansJson": {},
        "nonMemberOnlySellingPlansJson": {"517603537":{"id":1177,"shop":"bethechangeswiss.myshopify.com","subscriptionId":100597969,"sellingPlanId":517603537,"enableMemberInclusiveTag":false,"memberInclusiveTags":"","enableMemberExclusiveTag":true,"memberExclusiveTags":""}},
        "widgetEnabled": true,
        "showTooltip" : true,
        "sortByDefaultSequence": false,
        "showSubOptionBeforeOneTime": false,
        "showStaticTooltip": false,
        "showAppstleLink": false,
        "sellingPlanTitleText" : "{{sellingPlanName}} ({{sellingPlanPrice}}\/plus Lieferung)",
        "oneTimePriceText" : "{{price}}",
        "selectedPayAsYouGoSellingPlanPriceText" : "{{price}}",
        "selectedPrepaidSellingPlanPriceText" : "{{pricePerDelivery}}",
        "selectedDiscountFormat" : "SAVE {{selectedDiscountPercentage}}",
        "manageSubscriptionBtnFormat" : "<a href='apps\/subscriptions' class='appstle_manageSubBtn' ><button class='btn' style='padding: 2px 20px'>Manage Subscription<\/button><a><br><br>",
        "manageSubscriptionUrl" : "apps\/subscriptions",
        "appstlePlanId": 3,
        "showCheckoutSubscriptionBtn": true,
        "disableLoadingJquery": true,
        "widgetEnabledOnSoldVariant": "false",
        "switchRadioButtonWidget": false,
        "appstlePlanName": "BUSINESS",
        "appstlePlanFeatures": {"subscriptionCount":10000,"analytics":true,"enableSubscriptionManagement":true,"enableDunningManagement":true,"enableCustomerPortalSettings":true,"enableShippingProfiles":true,"enableProductSwapAutomation":false,"enableAdvancedSellingPlans":true,"enableSummaryReports":true,"enableCustomEmailDomain":false,"enableWidgetPlacement":true,"enableIntegrations":true,"enableSmsAlert":false,"enableCustomEmailHtml":true,"enableCancellationManagement":true,"enableBundling":false,"enableExternalApi":false,"enableCartWidget":false,"enableAutoSync":false},
        "formMappingAttributeName": "",
        "formMappingAttributeSelector": "",
        "quickViewModalPollingSelector": "",
        "scriptLoadDelay": "0",
        "formatMoneyOverride": "false",
        "appstle_app_proxy_path_prefix": "apps\/subscriptions",
        "updatePriceOnQuantityChange": "",
        "widgetParentSelector": "",
        "quantitySelector": "",
        "enableAddJSInterceptor": "false",
        "reBuyEnabled": "false",
        "widgetTemplateHtml": ``,
        "css": {
            "appstle_subscription_widget": {
                "margin-top": "" ,
                "margin-bottom": "",
            },

            "appstle_subscription_wrapper": {
                "border-width": "",
                "border-color": "",
            },

            "appstle_circle": {
                "border-color": "",
            },

            "appstle_dot": {
                "background-color": "",
            },

            "appstle_select": {
                "padding-top": "",
                "padding-bottom": "",
                "padding-left": "",
                "padding-right": "",
                "border-width": "",
                "border-style": "",
                "border-color": "",
                "border-radius": "",
            },

            "tooltip_subscription_svg": {
                "fill": "",
            },

            "appstle_tooltip": {
                "color": "",
                "background-color": "",
            },

            "appstle_tooltip_border_top_color": {
                "border-top-color": "",
            },

            "appstle_subscription_final_price": {
                "color": "",
            },
            "appstle_widget_text_color": {
                "color": "",
            },
            "appstle_selected_background": {
                "background": "transparent",
            },
            "customCSS": "",
            "customerPortalCss": "",
            "priceSelector": "",
            "landingPagePriceSelector": "",
            "quickViewClickSelector": "",
            "badgeTop": "",
            "pricePlacement": "BEFORE"
        }
      };

    }

    function urlIsProductPage() {
    // return null != decodeURIComponent(window.location.pathname).match(/\/products\/(([a-zA-Z0-9]|[\-\.\_\~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[\ud83c\ud83d\ud83e][\ud000-\udfff]){1,})\/?/)
    return decodeURIComponent(window.location.pathname).includes('/products/');
    }
  }
)(window);
